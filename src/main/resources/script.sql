CREATE SCHEMA `post-tracker`
  DEFAULT CHARACTER SET utf8;
USE `post-tracker`;

CREATE TABLE `Country` (
  `id`   INT          NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `Town` (
  `id`         INT          NOT NULL,
  `name`       VARCHAR(255) NOT NULL,
  `country_id` INT          NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `Town_country_id_fk_idx` (`country_id` ASC),
  CONSTRAINT `Town_country_id_fk` FOREIGN KEY (`country_id`) REFERENCES `Country` (`id`)
);

CREATE TABLE `Person` (
  `id`   INT          NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `Place` (
  `id`         INT          NOT NULL,
  `country_id` INT          NOT NULL,
  `town_id`    INT          NOT NULL,
  `address`    VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `Place_country_id_fk_idx` (`country_id` ASC),
  INDEX `Place_town_id_fk_idx` (`town_id` ASC),
  CONSTRAINT `Place_country_id_fk` FOREIGN KEY (`country_id`) REFERENCES `Country` (`id`),
  CONSTRAINT `Place_town_id_fk` FOREIGN KEY (`town_id`) REFERENCES `Town` (`id`)
);

CREATE TABLE `Parcel` (
  `id`                 INT          NOT NULL,
  `name`               VARCHAR(255) NOT NULL,
  `from_place_id`      INT          NULL,
  `to_place_id`        INT          NULL,
  `owner_id`           INT          NULL,
  `currentCoordinates` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  INDEX `Place_from_place_id_fk_idx` (`from_place_id` ASC),
  INDEX `Place_to_place_id_fk_idx` (`to_place_id` ASC),
  INDEX `Place_owner_id_fk_idx` (`owner_id` ASC),
  CONSTRAINT `Place_from_place_id_fk` FOREIGN KEY (`from_place_id`) REFERENCES `Place` (`id`),
  CONSTRAINT `Place_to_place_id_fk` FOREIGN KEY (`to_place_id`) REFERENCES `Place` (`id`),
  CONSTRAINT `Place_owner_id_fk` FOREIGN KEY (`owner_id`) REFERENCES `Person` (`id`)
);

# Inserts

INSERT INTO
  `Country` (`id`, `name`)
VALUES
  (1, 'Россия'),
  (2, 'Китай'),
  (3, 'Норвегия'),
  (4, 'Казахстан'),
  (5, 'Германия');

INSERT INTO
  `Town` (`id`, `name`, `country_id`)
VALUES
  (1, 'Москва', 1),
  (2, 'Санкт-Перербург', 1),
  (3, 'Петропавловск-Камчатский', 1),
  (4, 'Екатеринбург', 1),
  (5, 'Казань', 1),

  (6, 'Пекин', 2),
  (7, 'Шанхай', 2),
  (8, 'Гуанчжоу', 2),
  (9, 'Сучжоу', 2),

  (10, 'Осло', 3),
  (11, 'Шиен', 3),
  (12, 'Берген', 3),

  (13, 'Алма-Ата', 4),
  (14, 'Астана', 4),
  (15, 'Караганда', 4),

  (16, 'Берлин', 5),
  (17, 'Франкфурт-на-Майне', 5),
  (18, 'Лейпциг', 5);

INSERT INTO
  Place (`id`, `country_id`, `town_id`, `address`)
VALUES
  (1, 1, 1, 'Красная площадь д.1'),
  (2, 1, 2, 'Дворцовая площадь д.1'),
  (3, 1, 4, 'ул. Ленина д.17');

INSERT INTO
  Person (`id`, `name`)
VALUES
  (1, 'Иванов Иван');

INSERT INTO
  Parcel (`id`, `name`, `from_place_id`, `to_place_id`, `owner_id`, `currentCoordinates`)
VALUES
  (1, 'Чайник', 2, 1, 1, '55.76,37.64'),
  (2, 'Кружка', 3, 1, 1, '56.13,40.40');
