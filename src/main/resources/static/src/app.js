window.onload = function () {

    /* Vue JS */

    Vue.component('parcel-info', {
        props: ['parcel'],
        template: '#parcel-info'
    });

    var app = new Vue({
        el: '#app',
        mounted: function () {
            var init = function () {
                this.myMap = new ymaps.Map("map", {
                    center: [62.70, 94.0],
                    zoom: 2,
                    controls: ['zoomControl', 'fullscreenControl']
                });
            }.bind(this);
            ymaps.ready(init);
        },
        data: {
            active: 1,
            myMap: null,
            parcel: null,
            parcelId: "1",
            area: [],
            parcelList: [],
            personList: [],
            owner: null
        },
        methods: {
            makeActive: function (item) {
                this.active = item;
                this.parcel = null;
                this.parcelList = [];
                this.myMap.geoObjects.removeAll();
                if (item === 2) {
                    this.findInArea();
                }
                if (item === 3) {
                    this.loadPersons();
                }
            },
            setMarker: function (parcel) {
                var coordinates = parcel.currentCoordinates.split(',').map(Number);
                var marker = new ymaps.Placemark(
                    coordinates, {
                        hintContent: parcel.id,
                        balloonContent: parcel.name
                    }
                );
                this.myMap.geoObjects.add(marker);
                return coordinates;
            },
            findParcel: function (parcelId) {
                parcelId = Number(parcelId);
                if (parcelId === NaN) {
                    alert("Указан некорректный идентификатор посылки!\nНеобходимо указывать только цифры.");
                    return
                }
                this.$http.get('/api/parcels/' + parcelId).then(response => {
                    if (response.body.id == undefined) {
                        alert("Посылка с идентификатором " + parcelId + " не найдена!");
                        return
                    }

                    this.parcel = response.body;

                    var coordinates = this.setMarker(this.parcel);
                    this.myMap.setCenter(coordinates, 6, {
                        checkZoomRange: true
                    });
                })
            },
            findInArea: function () {
                this.myMap.geoObjects.removeAll();
                var polyline = new ymaps.Polyline([], {}, {
                    editorDrawingCursor: "crosshair",
                    editorMaxPoints: 2,
                    strokeWidth: 0
                });
                this.myMap.geoObjects.add(polyline);

                polyline.editor.startDrawing();

                polyline.editor.events.add('drawingstop', function () {
                    this.area = polyline.geometry.getCoordinates();

                    if (!_.isEmpty(this.area)) {
                        myRectangle = new ymaps.Rectangle(this.area, {}, {
                            fillColor: '#7df9ff33',
                            fillOpacity: 0.5,
                            strokeColor: '#0000FF',
                            strokeOpacity: 0.5,
                            strokeWidth: 2
                        });
                        this.myMap.geoObjects.add(myRectangle);

                        // polyline.editor.state.set('editing', false); // Cannot read property 'getLength' of null

                        var getMax = function (o1, o2) {
                            return o1 > o2 ? o1 : o2;
                        };
                        var getMin = function (o1, o2) {
                            return o1 < o2 ? o1 : o2;
                        };

                        var rect = {
                            x1: getMin(this.area[0][0], this.area[1][0]),
                            x2: getMax(this.area[0][0], this.area[1][0]),
                            y1: getMax(this.area[0][1], this.area[1][1]),
                            y2: getMin(this.area[0][1], this.area[1][1])
                        };
                        this.$http.post('/api/parcels/area', rect).then(function (response) {

                            if (_.isEmpty(response.body)) {
                                alert("В указанной области не найдено посылок!");
                                return
                            }
                            this.parcelList = response.body;
                            _.each(this.parcelList, function (parcel) {
                                this.setMarker(parcel);
                            }.bind(this))

                        }.bind(this));
                    }

                }.bind(this));
            },
            loadPersons: function () {
                if (_.isEmpty(this.personList)) {
                    this.$http.get('/api/persons/list.json').then(response => {
                        this.personList = response.body;
                    })
                }
            },
            findByOwner: function (owner) {
                this.$http.get('/api/parcels/owner/' + owner.id).then(response => {
                    if (_.isEmpty(response.body)) {
                        alert ("У выбранного получателя не найдено посылок!");
                        return
                    }
                    this.parcelList = response.body;
                    _.each(this.parcelList, function (parcel) {
                        this.setMarker(parcel);
                    }.bind(this))
                })
            }
        }
    });

};