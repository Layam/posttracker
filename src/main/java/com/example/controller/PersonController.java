package com.example.controller;


import com.example.bean.Person;
import com.example.dao.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = "/api/persons")
public class PersonController {

    @Autowired
    private PersonRepository personRepository;

    @RequestMapping(value = "/list.json", method = RequestMethod.GET)
    @ResponseBody
    public List<Person> personList() {
        return (List<Person>) personRepository.findAll();
    }

}
