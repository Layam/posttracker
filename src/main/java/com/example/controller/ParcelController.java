package com.example.controller;

import com.example.bean.Parcel;
import com.example.dao.ParcelRepository;
import com.example.utils.Area;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/api/parcels")
public class ParcelController {

    @Autowired
    private ParcelRepository parcelRepository;


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Parcel getById(@PathVariable Integer id) {
        return parcelRepository.findOne(id);
    }

    @RequestMapping(value = "/area", method = RequestMethod.POST)
    @ResponseBody
    public List<Parcel> getInArea(@RequestBody Area area) {
        ArrayList<Parcel> result = new ArrayList<>();

        Iterable<Parcel> all = parcelRepository.findAll();
        all.forEach(parcel -> {
            String[] coordinates = parcel.getCurrentCoordinates().split(",");
            if (area.getX1().compareTo(Double.valueOf(coordinates[0])) < 0
                    && area.getX2().compareTo(Double.valueOf(coordinates[0])) > 0
                    && area.getY1().compareTo(Double.valueOf(coordinates[1])) > 0
                    && area.getY2().compareTo(Double.valueOf(coordinates[1])) < 0) {
                result.add(parcel);
            }
        });

        return result;
    }

    @RequestMapping(value = "/owner/{id}", method = RequestMethod.GET)
    @ResponseBody
    public List<Parcel> getByOwner(@PathVariable Integer id) {
        return parcelRepository.findByOwnerId(id);
    }
}
