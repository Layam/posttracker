package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PostTrackerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PostTrackerApplication.class, args);
	}
}

/*
	@Configuration
	@ComponentScan({"ru.vetrf.vesta.web"})
	@EnableWebMvc
	public class WebConfiguration extends WebMvcConfigurerAdapter

	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        String webappDir = appProperties().getProperty("webapp.dir");
        registry.addResourceHandler("/app/**")
                .addResourceLocations(webappDir + "/app/", webappDir + "/app/templates/")
                .setCachePeriod(60 * 60 * 2);
    }
*/