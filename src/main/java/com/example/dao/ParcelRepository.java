package com.example.dao;

import com.example.bean.Parcel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ParcelRepository extends CrudRepository<Parcel, Integer> {

    Parcel findParcelById(Integer id);

    List<Parcel> findByOwnerId(Integer ownerId);

}
