package com.example.dao;

import com.example.bean.Town;
import org.springframework.data.repository.CrudRepository;

public interface TownRepository extends CrudRepository<Town, Integer> {
}
