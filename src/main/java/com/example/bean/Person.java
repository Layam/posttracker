package com.example.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "Person")
public class Person {
    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    @JsonIgnore
    @OneToMany(mappedBy = "owner")
    private List<Parcel> parcelList;
}
