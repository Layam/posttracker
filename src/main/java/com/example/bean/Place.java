package com.example.bean;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Place")
public class Place {
    @Id
    @GeneratedValue
    private Integer id;
    @ManyToOne
    @JoinColumn(name="town_id")
    private Town town;
    private String address;
}
