package com.example.bean;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Parcel")
public class Parcel {
    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    private String currentCoordinates;
    @ManyToOne
    @JoinColumn(name="from_place_id")
    private Place from;
    @ManyToOne
    @JoinColumn(name="to_place_id")
    private Place to;
    @ManyToOne
    @JoinColumn(name="owner_id")
    private Person owner;

    public String getCurrentCoordinates() {
        return currentCoordinates;
    }
}
