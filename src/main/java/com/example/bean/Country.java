package com.example.bean;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "Country")
public class Country {
    @Id
    @GeneratedValue
    private Integer id;
    private String name;
//    @JsonIgnore
//    @OneToMany
//    @JoinColumn(name = "country_id")
//    private List<Town> townList;
}
